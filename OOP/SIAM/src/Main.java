/*KELAS MAIN*/

public class Main {
    public static void main(String[] args) {
        Mahasiswa mhs1 = new Mahasiswa("Vani", 1234, "Sistem Informasi", 2021);
        Mahasiswa mhs2 = new Mahasiswa("Bona", 5678, "Teknologi Informasi", 2022);

        Kursus mk1 = new Kursus("Pemrograman Lanjut", 1001, 5);
        Kursus mk2 = new Kursus("Pemrograman Web", 1002, 3);

        Transkrip t1 = new Transkrip(mhs1, mk1, 90);
        Transkrip t2 = new Transkrip(mhs1, mk2, 92);
        Transkrip t3 = new Transkrip(mhs2, mk1, 89);
        Transkrip t4 = new Transkrip(mhs2, mk2, 92);

        mhs1.printTranscripts();
        mhs2.printTranscripts();
    }
}
